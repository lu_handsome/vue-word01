import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import VueResource from 'vue-resource'
import Words from '../components/Words'
import UpdWord from '../components/UpdWord'
import AddWord from '../components/AddWord'
Vue.use(Router)
Vue.use(VueResource)

export default new Router({
  routes: [
    {
      path:'/',
      redirect:'words'
    },
    {
      path: '/helloworld',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/words',
      component: Words
    },
    {
      path:'/updword',
      name:'UpdWord',
      component:UpdWord
    },
    {
      path:'/addword',
      component:AddWord
    }
  ]
})
