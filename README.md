# word01

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
效果展示：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083410_f7814f8f_5405887.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083431_72a5e377_5405887.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083444_e261b355_5405887.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083506_8c6bd6a1_5405887.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083534_76642d25_5405887.png "屏幕截图.png")